tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dec(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)    -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)    -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)    -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)    -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr)    -> assign(p1={$e1.st},p2={$e2.st})
        | INT                         -> int(i={$INT.text})
        | ID                          -> id(name={$ID.text})
        | ^(EQUAL e1=expr e2=expr)    -> compare(p1={$e1.st},p2={$e2.st})
        | ^(NOTEQUAL e1=expr e2=expr) -> compare(p1={$e1.st},p2={$e2.st})
        | ^(IF e1=expr e2=expr e3=expr?) {numer++;}   -> if(e1={$e1.st},e2={$e2.st},e3={$e3.st},id={numer.toString()})
        | ^(WHILE e1=expr e2=expr){numer++;}          -> while(e1={$e1.st},e2={$e2.st},id={numer.toString()})
        | ^(DO e1=expr e2=expr){numer++;}             -> doWhile(e1={$e1.st},e2={$e2.st},id={numer.toString()})
    ;
    