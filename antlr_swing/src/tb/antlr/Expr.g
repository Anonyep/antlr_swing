grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat)+ EOF!;

stat
    : expr NL -> expr
    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
    | ID PODST expr NL -> ^(PODST ID expr)
    | condStatement NL -> condStatement
    | whileLoop NL -> whileLoop
    | NL ->
    ; 

condStatement
      : IF^ LP! expr RP! THEN! (expr) (ELSE! (expr))?;
      
whileLoop
    : (WHILE^ expr expr)
    | (DO^ expr WHILE! expr)
    ;

expr
    : arithmeticExpr
      ( EQUAL^ arithmeticExpr
      | NOTEQUAL^ arithmeticExpr
      )*
    ;
    
arithmeticExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )* 
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR 
  :'var'
  ;
  
WHILE
  :'while'
  ;

DO
  :'do'
  ;

IF
  :'if'
  ;

ELSE
  :'else'
  ;

THEN
  : 'then'
  ;
  
EQUAL : '==';

NOTEQUAL : '!=';

BEGIN 
  : '{'
  ;
  
END
  : '}'
  ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
  : '('
  ;

RP
  : ')'
  ;

PODST
  : '='
  ;

PLUS
  : '+'
  ;

MINUS
  : '-'
  ;

MUL
  : '*'
  ;

DIV
  : '/'
  ; 